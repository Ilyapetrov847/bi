use std::io::prelude::*;
use std::net::TcpListener;
use std::net ::TcpStream;
use std::fs;

fn main() {
  let listener = TcpListener::bind("127.0.0.1:8080").unwrap();
  for stream in listener.incoming(){
      let stream = stream.unwrap();
      println!("Connected: {:?}", stream);
      handle_connection(stream);
  }
}
fn handle_connection(mut stream: TcpStream) {
  let mut buf = [0; 1024];
  stream.read(&mut buf).unwrap();
  let buf_get = b"GET / HTTP/1.1\r\n";
  if buf.starts_with(buf_get) {
      let content = fs::read_to_string("index.html").unwrap();
      let res = format!("HTTP/1.1 200 OK\r\nContent-Length: {}r\n\r\n{}", content.len(), content);
      stream.write(res.as_bytes()).unwrap();
      stream.flush().unwrap();
  }
}
