FROM gitpod/workspace-full
# https://www.gitpod.io/docs/config-docker/
# cf. https://hub.docker.com/r/gitpod/workspace-full/dockerfile

ENV PATH="${HOME}/.cargo/bin:${PATH}"

RUN \
  wget -O- https://sh.rustup.rs > /tmp/rustup-init.sh &&\
  sh /tmp/rustup-init.sh -y --default-toolchain none &&\
  ls -laF $HOME/.cargo/bin &&\
  rustup set profile minimal &&\
  rustup default nightly-2020-09-24 &&\
  rustup component add rust-analysis rust-src
