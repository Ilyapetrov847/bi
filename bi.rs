#![feature(non_ascii_idents)]
#![allow(unknown_lints, uncommon_codepoints)]

#![allow(non_snake_case)]

#[macro_use] extern crate fomat_macros;
#[macro_use] extern crate gstuff;
#[macro_use] extern crate serde_derive;

use chrono::{DateTime, Local, TimeZone};
use chrono::format::DelayedFormat;
use chrono::format::strftime::StrftimeItems;
use gstuff::{binprint, now_float, now_ms, slurp, status_line, status_line_clear, rdtsc, with_status_line, ISATTY};
use hmac::{Hmac, Mac, NewMac};
use inlinable_string::{InlinableString, StringExt};
use once_cell::sync::OnceCell;
use pico_args::Arguments;
use serde_json::{self as json, Value as Json};
use serde_json::value::{RawValue as RawJson};
use std::borrow::Cow;
use std::fs;
use std::fmt::Write as FmtWrite;
use std::io::{self, Write};
use std::str::from_utf8_unchecked;
use std::sync::atomic::{AtomicI64, AtomicU64, Ordering};
use std::thread;
use std::time::Duration;
use tungstenite::{connect, Message};
use url::Url;

macro_rules! status {($($args: tt)+) => {if *ISATTY {
  status_line (file!(), line!(), fomat! ($($args)+))}}}

fn short_log_time (ms: u64) -> DelayedFormat<StrftimeItems<'static>> {
  let time = Local.timestamp_millis (ms as i64);
  time.format ("%d %H:%M:%S")}

macro_rules! log {($($args: tt)+) => {{
  with_status_line (&|| {
    pintln! (
      ($crate::short_log_time (::gstuff::now_ms())) ' '
      (::gstuff::filename (file!())) ':' (line!()) "] "
      $($args)+)})}}}

/// Fomat into InlinableString
macro_rules! ifomat {
  ($($args: tt)+) => ({
    let mut is = InlinableString::new();
    use std::fmt::Write;
    wite! (&mut is, $($args)+) .expect ("!wite");
    is})}

fn digit2hex (num: u8) -> char {if num < 10 {(b'0' + num) as char} else {(b'a' + num - 10) as char}}

/// Difference between local and Binance server time  
/// In milliseconds, but HTTP header is in seconds
static BI_DELTA: AtomicI64 = AtomicI64::new (0);

/// Monitor Binance HTTP headers in order to simulate a time sync
fn bi_timesync (res: &attohttpc::Response) -> Result<(), String> {
  if let Some (date) = res.headers().get ("date") {
    let date = try_s! (DateTime::parse_from_rfc2822 (try_s! (date.to_str())));
    let now = date.timezone().timestamp_millis (now_ms() as i64);
    let delta = (now - date) .num_milliseconds();
    let was = BI_DELTA.load (Ordering::Relaxed);
    let average = if was == 0 {delta} else {(was * 3 + delta) / 4};
    //log! ([=delta] ' ' [=average]);
    BI_DELTA.store (average, Ordering::Relaxed)}
  Ok(())}

/// Check if Biance is operational. Updates the time delta too
fn check_bi_status() -> Result<(), String> {
  // https://binance-docs.github.io/apidocs/spot/en/#system-status-system
  let res = try_s! (attohttpc::get ("https://api.binance.com/wapi/v3/systemStatus.html") .send());
  if !res.is_success() {return ERR! ("!200: {}", res.status())}
  try_s! (bi_timesync (&res));
  let res: Json = try_s! (res.json_utf8());
  if res["msg"].as_str() != Some ("normal") {return ERR! ("!status: {}", res.to_string())}
  Ok(())}

fn hv<'a> (hm: &'a http::header::HeaderMap, hn: &str) -> &'a str {
  if let Some (hv) = hm.get (hn) {
    if let Ok (hv) = hv.to_str() {
      hv
    } else {""}
  } else {""}}

static BK: OnceCell<(String, String)> = OnceCell::new();
fn bk() -> Result <(&'static str, &'static str), String> {
  let tup = try_s! (BK.get_or_try_init (|| {
    let bodice = try_s! (String::from_utf8 (slurp (&"db/key")));
    let vec: Vec<&str> = bodice.split_ascii_whitespace().collect();
    if vec.is_empty() || vec[0] != "key,sk" {return ERR! ("!key,sk")}
    Ok ((vec[1].to_string(), vec[2].to_string()))}));
  Ok ((&tup.0, &tup.1))}

/// Perform a Binance request
fn bi (ts: bool, sig: bool, post: bool, path: &str, label: &str, qs: &str) -> Result<Vec<u8>, String> {
  let mut attempts = 0;
  let (succ, bulk) = loop {
    attempts += 1;

    let mut qs = if ts {
      let delta = BI_DELTA.load (Ordering::Relaxed);
      let timestamp = now_ms() as i64 - delta;
      //log! ([=delta] ' ' [=timestamp]);
      fomat! (
        (qs)
        if !qs.is_empty() {'&'}
        "timestamp=" (timestamp)
        "&recvWindow=22111")  // Need a larger time window to account for flaky time sync
    } else {qs.to_string()};

    let bk = try_s! (bk());
    // NB: For some reason it's different from `echo 'qs' | openssl dgst -sha256 -hmac sk`
    let mut mac = try_s! (Hmac::<sha2::Sha256>::new_varkey (bk.1.as_bytes()));
    mac.update (qs.as_bytes());
    let mac = mac.finalize().into_bytes();

    let mut http = attohttpc::Session::new();
    // https://github.com/sbstp/attohttpc/pull/80/files
    // adb forward tcp:8080 tcp:8080
    // set http_proxy=http://127.0.0.1:8080/
    // set https_proxy=http://127.0.0.1:8080/
    http.proxy_settings (attohttpc::ProxySettings::from_env());

    let res = if post {
      if sig {
        try_s! (wite! (qs, "&signature=" for u in mac {(digit2hex (u / 16)) (digit2hex (u % 16))}))}
      let url = fomat! ("https://api.binance.com/" (path));
      let req = http.post (url) .text (qs) .header ("X-MBX-APIKEY", bk.0);
      try_s! (req.send())
    } else {
      let url = fomat! (
        "https://api.binance.com/" (path) "?" (qs)
        // “Endpoints use HMAC SHA256 signatures.
        // The HMAC SHA256 signature is a keyed HMAC SHA256 operation.
        // Use your secretKey as the key and totalParams as the value for the HMAC operation.
        // totalParams is defined as the query string concatenated with the request body”
        if sig {
          "&signature=" for u in mac {(digit2hex (u / 16)) (digit2hex (u % 16))}});

        let req = http.get (&url) .header ("X-MBX-APIKEY", bk.0);
        try_s! (req.send())};

    try_s! (bi_timesync (&res));

    // NB: “The limits on the API are based on the IPs, not the API keys”
    // https://binance-docs.github.io/apidocs/spot/en/#limits
    //log! ("x-mbx-used-weight: " (hv (res.headers(), "x-mbx-used-weight")));
    //log! ("x-mbx-used-weight-1m: " (hv (res.headers(), "x-mbx-used-weight-1m")));
    let retry_after = hv (res.headers(), "Retry-After");
    if let Ok (sec) = retry_after.parse::<u64>() {
      log! ("Retry-After: " (sec));
      thread::sleep (Duration::from_secs (sec + 1));
      continue}

    let succ = res.is_success();
    let bulk = try_s! (res.bytes());

    // Retry automatically whenever there is a timestamp problem
    let ahead = "\"Timestamp for this request was 1000ms ahead of the server's time.\"";
    let bulkˢ = unsafe {from_utf8_unchecked (&bulk)};
    if !succ && bulkˢ.contains (ahead) && attempts < 7 {continue}

    let try_again = "\"Internal error; unable to process your request. Please try again.\"";
    if !succ && bulkˢ.contains (try_again) && attempts < 7 {continue}

    break (succ, bulk)};

  if !label.is_empty() {
    let mut sf = try_s! (fs::File::create (fomat! ("api-samples/" (label) ".json")));
    try_s! (sf.write_all (&bulk))}
  if !succ {return ERR! ("!{}: {}", path, binprint (&bulk, b'.'))}
  Ok (bulk)}

#[derive (Debug, Deserialize)]
struct BiBalance {
  asset: InlinableString,
  free: InlinableString,
  locked: InlinableString}

#[derive (Debug, Deserialize)]
struct BiAccount {
  makerCommission: u8,
  takerCommission: u8,
  balances: Vec<BiBalance>}

/// Get current holdings (vertices)
fn get_bi_account() -> Result<BiAccount, String> {
  let res = try_s! (bi (true, true, false, "api/v3/account", "account", ""));
  let bodice: BiAccount = try_s! (json::from_slice (&res));
  Ok (bodice)}

#[derive (Debug)]
struct BiKline {
  symbol: InlinableString,
  hour: u32,
  open: f64,
  high: f64,
  low: f64,
  close: f64,
  volume: f64,
  trades: u32}

fn bi_klines (ta: &str, tb: &str) -> Result<Vec<BiKline>, String> {
  // https://binance-docs.github.io/apidocs/spot/en/#kline-candlestick-data
  // https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#klinecandlestick-data
  let now = now_ms();
  let res = try_s! (bi (false, false, false, "api/v3/klines", &fomat! ("klines-" (ta) '-' (tb)), &fomat! (
    "symbol=" (ta) (tb)
    // cf. “Kline/Candlestick chart intervals”
    // “m -> minutes; h -> hours; d -> days; w -> weeks; M -> months”
    "&interval=1h"
    "&startTime=" (now - 24 * 3600 * 1000)
    "&endTime=" (now)
    "&limit=24")));
  let res: Vec<Vec<Json>> = try_s! (json::from_slice (&res));
  let mut klines = Vec::with_capacity (res.len());
  for en in res {
    macro_rules! jsp {
      ($js: expr) => (
        try_s! (try_s! ($js.as_str().ok_or ("!str")) .parse()))}
    klines.push (BiKline {
      symbol: ifomat! ((ta) (tb)),
      hour: (try_s! (en[0].as_u64().ok_or ("!time")) / 1000 / 3600) as u32,
      open: jsp! (en[1]),
      high: jsp! (en[2]),
      low: jsp! (en[3]),
      close: jsp! (en[4]),
      volume: jsp! (en[5]),
      trades: try_s! (en[8].as_u64().ok_or ("!trades")) as u32})}
  Ok (klines)}

/// https://github.com/binance-exchange/binance-official-api-docs/blob/master/web-socket-streams.md#individual-symbol-book-ticker-streams
#[derive (Debug, Deserialize)]
struct BookTicker {
  /// Symbol
  s: InlinableString,
  /// Best bid price (buying, green)
  b: InlinableString,
  /// Best ask price (selling, red)
  a: InlinableString}

/// https://github.com/binance-exchange/binance-official-api-docs/blob/master/web-socket-streams.md#trade-streams
#[derive (Debug, Deserialize, Serialize)]
struct Trade {
  #[serde (rename = "e")]
  event_type: InlinableString,

  #[serde (rename = "E")]
  event_time: u64,

  #[serde (rename = "s")]
  symbol: InlinableString,

  #[serde (rename = "t")]
  trade_id: u64,

  #[serde (rename = "p")]
  price: InlinableString,

  #[serde (rename = "q")]
  quantity: InlinableString,

  #[serde (rename = "b")]
  buyer_order_id: u64,

  #[serde (rename = "a")]
  seller_order_id: u64,

  #[serde (rename = "T")]
  trade_time: u64,

  #[serde (rename = "m")]
  market_maker: bool}

/// https://github.com/binance-exchange/binance-official-api-docs/blob/master/web-socket-streams.md#diff-depth-stream
#[derive (Debug, Deserialize, Serialize)]
struct DepthDiff {
  #[serde (rename = "e")]
  event_type: InlinableString,

  #[serde (rename = "E")]
  event_time: u64,

  #[serde (rename = "s")]
  symbol: InlinableString,

  /// First update ID in event
  #[serde (rename = "U")]
  first_update_id: u64,

  /// Final update ID in event
  #[serde (rename = "u")]
  final_update_id: u64,

  #[serde (rename = "b")]
  bids: Vec<(InlinableString, InlinableString)>,

  #[serde (rename = "a")]
  asks: Vec<(InlinableString, InlinableString)>}

fn mainʹ() -> Result<(), String> {
  let mut args = Arguments::from_env();
  let symbol = try_s! (try_s! (args.free()) .pop().ok_or ("!symbol"));

  status! ("Checking if Binance is currently online…");
  try_s! (check_bi_status());

  status! ("Subscribing to Binance websocket updates…");
  // https://github.com/binance-exchange/binance-official-api-docs/blob/master/web-socket-streams.md
  let url = try_s! (Url::parse ("wss://stream.binance.com:9443/stream"));
  let (mut socket, response) = try_s! (connect (url));
  if response.status().as_u16() != 101 {
    return ERR! ("Unexpected HTTP code: {}", response.status())}

  let subscribe_id = 1;
  // Getting both the “aggTrade” and “trade” for exploration' sake;
  // “trade” might be preferable as it likely allows us to find our own orders in the feed
  let symbolˡ = symbol.to_ascii_lowercase();
  #[derive(Serialize)] struct Subscribe {
    method: InlinableString,
    params: Vec<InlinableString>,
    id: u32}
  let subscribe = try_s! (json::to_string (&Subscribe {
    method: "SUBSCRIBE".into(),
    params: vec! [
      ifomat! ((symbolˡ) "@aggTrade"),
      ifomat! ((symbolˡ) "@trade"),
      ifomat! ((symbolˡ) "@bookTicker"),
      ifomat! ((symbolˡ) "@depth")],
    id: 1}));
  try_s! (socket.write_message (Message::Text (subscribe)));

  let mut book_ticker: Option<BookTicker> = None;
  loop {
    status! ("loop…");
    let msg = try_s! (socket.read_message());
    match msg {
      Message::Text (text) => {
        #[derive (Debug, Deserialize)] struct WsResult<'a> {
          #[serde(borrow)] result: &'a RawJson,
          id: u32}
        if let Ok (result) = json::from_str::<WsResult> (&text) {
          if result.id == subscribe_id {status_line_clear(); continue} else {log! ([=result])}}

        log! ((text));

        #[derive (Debug, Deserialize)] struct WsStream<'a> {
          #[serde(borrow)] stream: Cow<'a, str>,
          #[serde(borrow)] data: &'a RawJson}
        if let Ok (stream) = json::from_str::<WsStream> (&text) {
          if !stream.stream.starts_with (&symbolˡ) {return ERR! ("!symbol: {}", stream.stream)}
          if stream.stream.ends_with ("@depth") {
            // TODO: Save the diffs into a time series structure in order to show them on the graphics
            // We might need to see backwards in time to get the actual diff, so {price: [second: diff]}
            //log! ("TODO; depth: " (text));
            let _diff: DepthDiff = try_s! (json::from_str (stream.data.get()));
            //log! ([=diff]);
          } else if stream.stream.ends_with ("@bookTicker") {
            book_ticker = Some (try_s! (json::from_str (stream.data.get())));
          } else if stream.stream.ends_with ("@aggTrade") {
            // TODO
            //log! ("TODO; aggTrade: " (text));
          } else if stream.stream.ends_with ("@trade") {
            let _trade: Trade = try_s! (json::from_str (stream.data.get()));
          } else {log! ([=stream])}
          continue}

        log! ((text))},
      Message::Ping (ping) => {
        try_s! (socket.write_message (Message::Pong (ping)))},
      Message::Close (close) => {
        return ERR! ("websocket being closed by server, {:?}", close)},
      _ => return ERR! ("websocket message {:?}", msg)}}}

fn main() {
  if let Err (err) = mainʹ() {
    epintln! ('\n' (err));
    std::process::exit (1)}}
